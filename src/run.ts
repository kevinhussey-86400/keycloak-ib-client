import KcAdminClient from '@keycloak/keycloak-admin-client';
import UserRepresentation from "@keycloak/keycloak-admin-client/lib/defs/userRepresentation";
const credentials = require('./credentials.json');
const usersJson = require('./users.json');

class Keycloak {
  private kcAdminClient = new KcAdminClient({
    baseUrl: 'https://keycloak.shrd.sparklecore.net/auth',
    realmName: '86400-non-prod-app',
  });

  async login(): Promise<void> {

    await this.kcAdminClient.auth(credentials);
  }

  async findAndUpdatePassword(cxeKeycloakId: string): Promise<void> {
    const users: UserRepresentation[] = await this.kcAdminClient.users.find({
      username: '971cff9c-4231-49c0-8253-545d71129c18'
    });

    users.forEach(async (user) => {
      if (user.id) {
        await this.kcAdminClient.users.resetPassword({
          id: user.id,
          credential: {
            temporary: false,
            type: 'password',
            value: 'PatNolan1!',
          }
        })
      }
    });
  }
}

const keycloak = new Keycloak();
const delay = (ms:number) => new Promise(resolve => setTimeout(resolve, ms));

const start = async function() {
  for (const kyId of usersJson) {
    try {

      await keycloak.login();
      await keycloak.findAndUpdatePassword(kyId);
      await delay(1000);
      console.log(`password updated for ${kyId}`);
    } catch (err) {
      console.log(`ERROR resetting password updated for ${kyId}`);
    }
  }
};

start();



